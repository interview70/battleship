package com.odigeo.interview.coding.battleshipservice.service;

import com.odigeo.interview.coding.battleshipservice.model.Cell;
import com.odigeo.interview.coding.battleshipservice.model.ship.Ship;
import com.odigeo.interview.coding.battleshipservice.model.ship.ShipType;
import com.odigeo.interview.coding.battleshipservice.util.GameConfiguration;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class FieldService {

    @Inject
    private CoordinateService coordinateService;

    public boolean allShipsSunk(Cell[][] field) {
        Map<ShipType, Integer> shipHitCount = new HashMap<>();
        Arrays.asList(ShipType.values()).stream().forEach(s -> shipHitCount.put(s, 0));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Cell cell = field[i][j];
                if (cell.getShip() != null && cell.isHit()) {
                    Integer shipCount = shipHitCount.get(cell.getShip().getShipType());
                    shipHitCount.put(cell.getShip().getShipType(), shipCount + 1);
                }
            }
        }
        return shipHitCount.entrySet()
                .stream()
                .allMatch(e -> e.getValue().equals(e.getKey().getShipLength()));
    }

    public boolean isShipSunk(Cell[][] field, Ship ship) {
        return ship.getCoordinates().stream().allMatch(cr -> field[cr.getRow()][cr.getColumn()].isHit());
    }

    public Cell[][] buildField(List<Ship> shipsDeployment) {
        Cell[][] field = buildWater();
        deployShips(field, shipsDeployment);
        return field;
    }

    private Cell[][] buildWater() {
        Cell[][] field = new Cell[GameConfiguration.FIELD_HEIGHT][GameConfiguration.FIELD_WIDTH];
        for (int row = 0; row < GameConfiguration.FIELD_HEIGHT; row++) {
            for (int col = 0; col < GameConfiguration.FIELD_WIDTH; col++) {
                field[row][col] = new Cell();
            }
        }
        return field;
    }

    private void deployShips(Cell[][] field, List<Ship> ships) {
        ships.forEach(ship ->
                ship.getCoordinates().forEach(coordinate ->
                        field[coordinate.getRow()][coordinate.getColumn()] = new Cell(ship)
                )
        );
    }

}
